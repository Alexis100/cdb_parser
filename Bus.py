class Bus():

    def __init__(self,name,type):
        #Variables que
        self._name = name
        self._type = type

    #Sobrecarga del str para printear
    def __str__(self):
        return ("Name : %s\nType : %s\n" %
                (self._name,self._type))
