import CContainer
class Device():

    def __init__(self,name,type,bus,address=False):
        #Variables que
        self._name = name
        self._type = type
        self._bus = bus
        self._address = address
        #List of ports which the device is source
        self._source = []
        #List of ports which the device is sink
        self._sink = []
        #List of all ports, sink + sources
        self._containers = []

    #Sobrecarga del str para printear
    def __str__(self):
        return ("Name : %s\nType : %s\nBus : %s\nAddress : %s\n" %
                (self._name,self._type,self._bus,self._address))

    def add_source_container(self,source_container):
        self._source.append(source_container)
        self._containers.append(source_container)
        return

    def add_sink_container(self,sink_container):
        self._sink.append(sink_container)
        self._containers.append(sink_container)
        return