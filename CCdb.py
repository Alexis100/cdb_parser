import re
import copy

import CDevice
import CContainer
import Variable
import Bus

class CCdb():

    def __init__(self,file_path):
        #Variables que
        self._file = file_path
        #Atributo que almacen la lista de variables de ethernet (sin las asociadas a los BINDDCONTAINER)
        self._eth_container_variables = self.build_eth_container_variables()
        #Se construyen los contenedores incluidos los bindcontainers y se actualizan las variables dentro
        self._eth_containers= self.build_eth_containers()
        #Atributo que almacena todas las variables (incluidas las de los CONTAINERS)
        self._eth_all_vars = self._get_all_eth_variables()
        #Atributo que almacena todos los devices
        self._eth_devices = self.build_eth_devices()
        """
        #Abributo que almacena el bus eth del cdb
        self._eth_bus = self.build_eth_bus()
        """
    #Metodo que devuelve el handler al fichero (NOT WORKING)
    def open_file(self,file):
        try:
            f = open(file,'r')
            yield f
        except IOError as ex:
            print ("Input/Ouput opening file\nError-->%s" % ex)
            raise
        except Exception as ex:
            print ("Exception opening file\nError-->%s" % ex)
            raise

    #Metodo que accede a las variables declaradas en el CDB (No estan incluidas las de los BINDCONTAINER)
    def build_eth_container_variables(self):
        #BUS_ETH_START_PATTERN = "ETH-PD(.*?)"
        #Diferentes patrones para diferentes cdbs
        #BUS_ETH_START_PATTERN = "ETH-PD(.*?)"
        BUS_ETH_START_PATTERN= "BUS ETH ETH-PD(.*?)"
        BUS_ETH_START_PATTERN = "BUS MVB"
        #BUS_ETH_START_PATTERN = "BUS ETHERNET ETH-PD(.*?)"
        #BUS_ETH_START_PATTERN = "BUS Ethernet ETH-PD(.*?)"
        BUS_ETH_START_PATTERN = "BUS (.*) ETH-PD"
        BUS_ETH_END_PATTERN = "VALIDITY"

        regexp = re.compile(BUS_ETH_START_PATTERN)

        # lista para almacenar los containers de ethernet
        list_eth_variables = []
        match = False
        file = open(self._file, 'r')
        for line in file:
            if re.search(BUS_ETH_START_PATTERN, line):
                match = True
                continue
            elif re.match(BUS_ETH_END_PATTERN, line):
                match = False
                continue
            elif match:
                r = re.compile('VARIABLE(.*?)')
                m = r.search(line)
                if m:
                    variable_info = line.split()
                    offset = variable_info[4][variable_info[4].find("[") + 1:variable_info[4].find("]")]
                    container_name = variable_info[4].split("[", 1)[0]
                    eth_variable = Variable.Variable(variable_info[1], variable_info[2], variable_info[3],
                                                     container_name, offset)
                    # print(str(eth_variable))
                    list_eth_variables.append(eth_variable)
        return list_eth_variables


    #Metodo que devuelve la lista de contenedores ethernet
    def build_eth_containers(self):
        #BUS_ETH_START_PATTERN = "BUS ETH ETH-PD(.*?)"
        BUS_ETH_START_PATTERN="BUS (.*) ETH-PD"
        #BUS_ETH_START_PATTERN = "BUS ETHERNET ETH-PD(.*?)"
        #BUS_ETH_START_PATTERN = "BUS Ethernet ETH-PD(.*?)"
        #BUS_ETH_START_PATTERN = "BUS MVB"
        BUS_ETH_END_PATTERN = "VARIABLE"

        # lista para almacenar los containers de ethernet
        list_eth_containers = []
        list_bind_container = []
        match = False
        file = open(self._file,'r')
        for line in file:
            if re.search(BUS_ETH_START_PATTERN,line):
                match = True
                list_bind_container = []
                continue
            elif re.match(BUS_ETH_END_PATTERN,line):
                match = False
                continue
            elif match:
                r = re.compile('CONTAINER(.*?)PERIODIC')
                m = r.search(line)
                if m:

                    container_info = line.split()
                    eth_container = CContainer.CContainer(container_info[1],container_info[3],container_info[4],
                                                          container_info[5],self._get_variables_from_eth_container(container_info[1]))

                    #print(str(eth_container))
                    list_eth_containers.append(eth_container)

                #Para los bindcontainer
                r_bind = re.compile('BINDCONTAINER')
                m2 = r_bind.search(line)
                if m2:
                    bind_container_info = line.split()
                    bind_container_name = bind_container_info[1]
                    bind_container_period = bind_container_info[2]
                    bind_container_port = bind_container_info[3]
                    for container in list_eth_containers:
                        if container._name ==bind_container_name:
                            bind_container = CContainer.CContainer(bind_container_name,bind_container_period,
                                                                   container._size,bind_container_port,copy.deepcopy(container._variables))
                            list_bind_container.append(bind_container)
                #Para los replace de los bindcontainer
                r_bind = re.compile('replace=')
                m3 = r_bind.search(line)
                if m3:
                    get_string = line.split()
                    old_device = re.search('"([^"]*)"', get_string[1]).group(1)
                    new_device = re.search('"([^"]*)"', get_string[2]).group(1)
                    list_bind_container[-1]._name = list_bind_container[-1]._name.replace(old_device,new_device)
                    for variable in list_bind_container[-1]._variables:

                        #Actualizo por cada variable de cada bindcontainer el nombre y el contenedor al que pertenece
                        #print (str(list_bind_container[-1]._name))
                        variable._name = variable._name.replace(old_device,new_device)
                        variable._container = variable._container.replace(old_device,new_device)
                        #print (str(list_bind_container[-1]))

        #Actualizo la lista de contenedores de ethernet
        list_eth_containers+=list_bind_container
        return list_eth_containers



    def build_eth_devices(self):
        DEVICE_PATTERN = "DEVICE(.*?)"
        SOURCE_PATTERN = "SOURCE(.*?)"
        SINK_PATTERN = "SINK(.*?)"
        list_eth_devices = []
        file = open(self._file, 'r')
        for line in file:
            if re.search(DEVICE_PATTERN, line):
                device_info=line.split()
                #Si el device es ETHERNET no hay numero de puerto
                if device_info[3] == "ETH":
                    device = CDevice.Device(device_info[1], device_info[2], device_info[3])
                else:
                    device = CDevice.Device(device_info[1], device_info[2], device_info[3],
                                       device_info[4])
                list_eth_devices.append(device)

            if re.search(SOURCE_PATTERN, line):
                source_info = line.split()
                if source_info[1] == "ETH":

                    for container_name in source_info[2:]:
                        list_eth_devices[-1].add_source_container(self.get_container(container_name))

            if re.search(SINK_PATTERN, line):
                sink_info = line.split()
                if sink_info[1] == "ETH":
                    for container_name in sink_info[2:]:
                        list_eth_devices[-1].add_sink_container(self.get_container(container_name))
        return list_eth_devices

    """
    def build_eth_bus(self):
        BUS_PATTERN = "BUS (.*?)"
        list_bus = []
        file = open(self._file, 'r')
        bus_type_lst = ["TCN-MVB","ETH-PD"]
        for line in file:
            if re.search(BUS_PATTERN, line):
                bus_info=line.split()
                #Se construyen buses, los tipos de buses permitidos son ETHPD, MVB,CAN,
                #SNMP
                if bus_info[2]. in bus_type_lst:
                    bus = Bus.Bus(bus_info[1],bus_info[2])
                    list_bus.append(bus)


        return list_bus
"""













#

    ################################################Auxiliar functions###############################################
    #Return a list of variables belonging to input "container". WITHOUT BINDCONTAINERS
    def _get_variables_from_eth_container(self,container_name):
        variables_container = []
        for variable in self._eth_container_variables:
            if container_name == variable._container:
                variables_container.append(variable)
        if not variables_container:
            print ("Container not found")
            return False
        else:
            return variables_container

    # Returns a list of all ethernet variables
    def _get_all_eth_variables(self):
        all_eth_vars = []
        for container in self._eth_containers:
            for variable in container._variables:
                all_eth_vars.append(variable)
        return all_eth_vars

    def _convert_list_to_dictionary(self,list):
        dict_vars = dict((x._name, x) for x in list)
        return dict_vars

    #####################################################FORUSERS#####################################################
    #####################################################GetDataMethodsForUSERS#######################################
    #Get variables in a list
    def get_all_variables(self):
        return self._eth_all_vars
    #Get variable as object
    def get_variable(self,variable_name):
        for variable in self._eth_all_vars:
            if variable_name == variable._name:
                return variable
        print ("Variable not found")
        return False
    #Get containers in a list
    def get_containers(self):
        return self._eth_containers

    #It takes as input the name of the container as string
    def get_container(self,container_name):
        flag = False
        for container in self._eth_containers:
            if container_name == container._name:
                return container
        if flag == False:
            print("Containr not found")
        return

    def get_variables_dictionary(self):
        return self._convert_list_to_dictionary(self._eth_all_vars)

    def get_containers_dictionary(self):
        return self._convert_list_to_dictionary(self._eth_containers)
    def get_devices_dictionary(self):
        return self._convert_list_to_dictionary(self._eth_devices)


    #########################################################PRINTING############################################
    # Print variable
    def print_variable(self, variable_name):
        for variable in self._eth_all_vars:
            if variable_name == variable._name:
                print(str(variable))
                return
        print("Variable not found")
        return False
    #Prints all ethernet variables
    def print_all_variables(self):
        for variable in self._eth_all_vars:
            print (str(variable))
        return
    #Print all ethernet containers
    def print_all_containers(self):
        for container in self._eth_containers:
            print(str(container))
        return

    #Prints variables belonging to input "container"
    def print_variables_from_container(self,container_name):
        flag = False
        for container in self._eth_containers:
            if container_name == container._name:
                flag = True
                for variable in container._variables:
                    print (str(variable))
        if not flag:
            print("Container not found")
            return
        else:
            return
    #Print characteristics of a container
    def print_container(self,container_name):
        flag = False
        for container in self._eth_containers:
            if container_name==container._name:
                print (str(container))
                flag = True
        if not flag:
            print ("Container not found")
            return
        else:
            return


    def print_sourceContainer_device(self, device_name):
        for device in self._eth_devices:
            if device._name == device_name:
                for source_port in device._source:
                    print (str(source_port))
        return

    def print_sinkContainer_device(self, device_name):
        for device in self._eth_devices:
            if device._name == device_name:
                for sink_port in device._sink:
                    print (str(sink_port))
        return